# Blunts WP – [Brunched](http://brunch.io) version of [JointsWP](www.jointswp.com) #

A modified version of JointsWP which uses brunch and npm, rather than gulp and bower.

- Source files live in a non-joints `app` folder, as per default [brunch setup](https://github.com/brunch/brunch/tree/master/docs#basics).
- Files & folders in `app/assets` will be copied across to the Blunts `/assets` folder, as per default [brunch behaviour](https://github.com/brunch/brunch/tree/master/docs#basics)
- Blunts (& Joints) use [Zurb Foundation Sites](http://foundation.zurb.com). By default only a basic set of foundation components are enabled. Look in [`app/scss/style.scss`](https://bitbucket.org/crafttheweb/bluntswp/src/3f525a71be7c5adaf92118435bd4cea306ab54ba/app/scss/style.scss?at=master&fileviewer=file-view-default) to enable any other parts of foundation you wish to use in your project. 
- Sass variables are kept in [`utils/_settings.scss`](https://bitbucket.org/crafttheweb/bluntswp/src/3f525a71be7c5adaf92118435bd4cea306ab54ba/app/scss/utils/_settings.scss?at=master&fileviewer=file-view-default), which also houses all of the foundation settings overrides

## Installing ##
Install using Composer

```
#!bash

composer require thefold/bluntswp
```

But you will need something along the lines of this in your composer.json file so that it gets installed in the right place


```
#!json

"extra": {
    "installer-paths": {
        "public/wp-content/themes/{$name}/": [
            "type:wordpress-theme"
        ],
        "public/wp-content/plugins/{$name}/": [
            "type:wordpress-plugin"
        ]
    }
}
```

### BEGIN / FIRST TIME ###

```
#!bash

npm install
```

### ONE OFF COMPILE ###

```
#!bash

brunch build
```

### ONE OFF COMPILE FOR PRODUCTION/RELEASE ###

```
#!bash

brunch build --production
```

### WATCH ###

```
#!bash

brunch watch
```

### working with git/gitignore ###

the following lines should be added to your gitignore, and you'll also need to create the folders and the .gitkeep files inside them

public/wp-content/themes/*/assets/css/*
!public/wp-content/themes/*/assets/css/.gitkeep
public/wp-content/themes/*/assets/images/*
!public/wp-content/themes/*/assets/images/.gitkeep
public/wp-content/themes/*/assets/js/*
!public/wp-content/themes/*/assets/js/.gitkeep


### working with SHIPIT ###

Below is an example of a shipit file which will trigger off brunch on deploy.
If you are changing the bluntswp folder/theme name, make sure you update the paths below

```
module.exports = function (shipit) {

    require('shipit-deploy')(shipit);
    require('shipit-shared')(shipit);

    shipit.initConfig({
        default: {
            workspace: '/tmp/example',
            repositoryUrl: 'git@bitbucket.org:example/example.co.nz.git',
            ignores: ['.git','bower_components','sass','node_modules'],
            keepReleases: 3,
            shallowClone: true,
            branch: 'master',
            shared: {
                dirs: ['public/wp-content/uploads', 'public/files'],
                overwrite: true
            }
        },
        staging: {
            servers: 'XXX.XXX.XX.XXX',
            deployTo: '/var/www/example.example.co.nz/',
        },
        live: {
            servers: 'XXX.XXX.XX.XXX',
            deployTo: '/var/www/example.co.nz/',
            keepReleases: 4
        }
    });

    shipit.blTask('composer:install', function() {
        return shipit.local('composer install --optimize-autoloader --prefer-dist ', {cwd:shipit.config.workspace});
    });
    
    shipit.blTask('npm:install',function(){
        return shipit.local('cd public/wp-content/themes/bluntswp; npm install', {cwd:shipit.config.workspace});
    });

    shipit.blTask('brunch:build', function() {
        return shipit.local('cd public/wp-content/themes/bluntswp; brunch build --production', {cwd:shipit.config.workspace});
    });

    shipit.task('reload-nginx', function () {
           if(shipit.environment == 'live') {
            return shipit.remote('sudo /usr/local/bin/service-php5-fpm-manager reload');
        }
    });
    
    shipit.on('fetched',function(){
       shipit.start('composer:install', 'npm:install', 'brunch:build');
    });
    
    shipit.on('cleaned', function () {
        if(shipit.environment == 'live') {
            return shipit.start('reload-nginx'); 
        }
    });
};
```


---

## NOTE the below info is regarding the original Joints WP, and is over-ridden by the info above. ##

Find out more here: www.jointswp.com  
Demo: www.jointswp.com/demo

### What is JointsWP?
JointsWP is a blank WordPress theme built with Foundation 6, giving you all the power and flexibility you need to build complex, mobile friendly websites without having to start from scratch.

Starting its humble life as a fork of the popular theme Bones, JointsWP is now the foundation of thousands of websites across the globe.

### What comes with JointsWP?
JointsWP comes pre-baked with all of the great features that are found in the Foundation framework – simply put, if it works in Foundation, it will work in JointsWP. The theme also includes:

- Foundation Navigation Options
- Grid archive templates
- Translation Support
- Bower and Gulp Support
- And much, much more!

### What tools do I need to use JointsWP?
You can use whatever you want – seriously. While the Sass version comes with Bower and Gulp support out of the box, you aren’t required to use those by any means. You can use CodeKit, Grunt, Compass or nothing at all. It’s completely up to you how you decide to build you theme – JointsWP won’t get in the way of your workflow.

### Getting Started With Gulp
- Install [node.js](https://nodejs.org).
- Using the command line, navigate to your theme directory
- Run npm install
- Run gulp to confirm everything is working

[Read more about how Gulp is used with JointsWP.](http://jointswp.com/docs/gulp/)