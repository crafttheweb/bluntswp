exports.config =
  # See http://brunch.io/#documentation for docs.
  paths:
    public: 'assets'

  files:
    javascripts:
      joinTo:
        'js/scripts.js': /^(app[\\/]js)/
        'js/vendor.js': /^(app[\\/]vendor)/ # uncomment this if you want to use the brunch vendor file/folder/system
    stylesheets:
      joinTo:
        'css/style.css':[
           /^(app[\\/]scss[\\/]style)/
        ]
        'css/login.css':[
           /^(app[\\/]scss[\\/]login)/
        ]

  modules:
    wrapper: false
    definition: false

  plugins:
    sass:
      debug: 'comments' # or set to 'debug' for the FireSass-style output
      mode: 'native' # set to 'native' to force libsass, or 'ruby' for.... ruby!
      allowCache: true

    postcss:
      processors: [
        require('autoprefixer')(['ie > 8', 'last 2 versions']),
      ]

  overrides:
    production:
      optimize: true
      sourceMaps: false
      plugins: autoReload: enabled: false