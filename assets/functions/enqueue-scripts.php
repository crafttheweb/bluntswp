<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

    // Removes WP version of jQuery
    // wp_deregister_script('jquery');
    
    // Load version controlled jquery in footer
    // wp_enqueue_script( 'jquery', get_template_directory_uri() . '/node_modules/jquery/dist/jquery.min.js', array(), '2.1.4', true );
    
    // Adding Vendor (including Foundation) scripts file in the footer
    wp_enqueue_script( 'site-vendor-js', get_template_directory_uri() . '/assets/js/vendor.js', array( 'jquery' ), '', true );
    
    // Adding custom scripts file in the footer
    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/js/scripts.js', array( 'jquery' ), '', true );
   
    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all' );

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);